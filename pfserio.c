/* 
 * File:   pfserio.c
 * Author: pf
 *
 * pf's serial I/O
 * 
 * Inspired on previous code by Paulo Pedreiras (pbrp@ua.pt)
 * 
 * Created on 15 April 2017, 21:52
 */

#include "pfserio.h"

int _mon_getc(int canblock) {

    // Reset Overrun Eror Flag - if set UART does not receive any chars
    if (U1STAbits.OERR)
        U1STAbits.OERR;

    if (canblock == 0) {
        if (U1STAbits.URXDA)
            return (int) U1RXREG;
    }
    else {
        while (!U1STAbits.URXDA);
        return (int) U1RXREG;
    }
    return -1;
}
