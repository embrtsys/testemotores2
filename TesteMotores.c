/* 
 * File:   TesteMotores.c
 * Author: pf
 *
 * Created on 15 April 2017, 18:09
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "pfserio.h"
#include "pwm.h"




/****************************************************************************
 *
 * Define the keys for user interface
 */
#define     KEY_PWM_ON          '1'
#define     KEY_PWM_OFF         '0'
#define     KEY_PWM_INC         'k'
#define     KEY_PWM_DEC         'j'
#define     KEY_PWM_UP          'l'
#define     KEY_PWM_DONW        'h'
#define     KEY_FREQ_UP         'i'
#define     KEY_FREQ_DONW       'm'
#define     KEY_PWM_DEFAULT     'r'

/****************************************************************************
 *
 * Default values for PWM
 */
#define     PWM_DEFAULT_DC      50
#define     PWM_DEFAULT_FREQ    10000

/****************************************************************************
 * 
 * Prototypes
 */
void print_menu(void);
void print_status(uint16_t , uint16_t);


/****************************************************************************
 *
 * Main
 */
int main(int argc, char** argv) {

    /*
     * pwm_frequency: PWM frequency in Hz
     */
    volatile uint16_t pwm_frequency = PWM_DEFAULT_FREQ;
    /*
     * pwm_dutycycle_val: PWM duty cycle, as a percentage. Holds a value from 
     *                      0 t0 100. 
     */
    volatile int16_t pwm_dutycycle_val = PWM_DEFAULT_DC; 

    char c;

    /* Configure serial port to use USART1*/
    init_serial();
    
    /* Configure outputs */
    TRISDbits.TRISD0 = 0;
    TRISDbits.TRISD4 = 0;
    TRISDbits.TRISD5 = 0;
    TRISDbits.TRISD6 = 0;
    
 
    INTEnableSystemMultiVectoredInt();
        
    /* Disable PWM, to be on the safe side... */
    disable_pwm();

    /* Main cycle */
    while (1) {
        
        /* Print system status and user interface menu */
        print_status(pwm_frequency, pwm_dutycycle_val);
        print_menu();

        /* Get user input... */
        do {
            c = _mon_getc(0);
        } while (c <= 0);
        
        /* ... and process it. */
        switch (c) {
            case KEY_PWM_ON:
                enable_pwm();
                setPWM(pwm_frequency, pwm_dutycycle_val);
                break;

            case KEY_PWM_OFF:
                disable_pwm();
                break;

            case KEY_FREQ_UP:
                pwm_frequency += 100;
                setPWM(pwm_frequency, pwm_dutycycle_val);
                break;

            case KEY_FREQ_DONW:
                pwm_frequency -= 100;
                setPWM(pwm_frequency, pwm_dutycycle_val);
                break;

            case KEY_PWM_INC:
                pwm_dutycycle_val++;
                if (pwm_dutycycle_val > 99) {
                    pwm_dutycycle_val = 99;
                }
                setPWM(pwm_frequency, pwm_dutycycle_val);
                break;

            case KEY_PWM_DEC:
                pwm_dutycycle_val--;
                if (pwm_dutycycle_val < 0) {
                    pwm_dutycycle_val = 0;
                }
                setPWM(pwm_frequency, pwm_dutycycle_val);
                break;

            case KEY_PWM_UP:
                pwm_dutycycle_val += 5;
                if (pwm_dutycycle_val > 99) {
                    pwm_dutycycle_val = 99;
                }
                setPWM(pwm_frequency, pwm_dutycycle_val);
                break;

            case KEY_PWM_DONW:
                pwm_dutycycle_val -= 5;
                if (pwm_dutycycle_val < 0) {
                    pwm_dutycycle_val = 0;
                }
                setPWM(pwm_frequency, pwm_dutycycle_val);
                break;
                
            case KEY_PWM_DEFAULT:
                pwm_dutycycle_val = PWM_DEFAULT_DC;
                pwm_frequency = PWM_DEFAULT_FREQ;
                setPWM(pwm_frequency, pwm_dutycycle_val);
                break;

        }
        
    }

    return (EXIT_SUCCESS);
}

/*
 * print_menu()
 * 
 * Prints the user interface menu. All keys are configurable. 
 * 
 */
void print_menu(void) {
    printf("%c/%c - Disable/Enable PWM\r\n", KEY_PWM_OFF, KEY_PWM_ON);
    printf("%c/%c - Increase/Decrease frequency by 100Hz\r\n", KEY_FREQ_UP, KEY_FREQ_DONW);
    printf("%c/%c - Increase/Decrease duty-cicle by 1%%\r\n", KEY_PWM_INC, KEY_PWM_DEC);
    printf("%c/%c - Increase/Decrease duty-cicle by 5%%\r\n", KEY_PWM_UP, KEY_PWM_DONW);
    printf("%c - Restore initial values\r\n", KEY_PWM_DEFAULT);
    printf("\r\n");

}

/*
 * print_status(pwm_frequency, pwm_dutycycle)
 * 
 * Prints the system status: 
 *  - PWM state: ON/OFF
 *  - Current PWM frequency (in Hz)
 *  - Current duty cycle (in percentage). 
 */
void print_status(uint16_t pwm_frequency, uint16_t pwm_dutycycle) {

    _mon_putc(033);
    printf("[0;0H");
    
    printf("State: ");
    if(get_pwm_state()==PWM_ON){
        printf("ON \t");
    }
    else{
        printf("OFF\t");
    }
    
    printf("PWM: Freq=%5d\tD.C.=%3d%%\r\n\r\n", pwm_frequency, pwm_dutycycle);
}
