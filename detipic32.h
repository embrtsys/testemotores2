/* 
 * File:   detipic32.h
 * Author: pf
 *
 * Contains several definitions related to the DETIPic32 board
 * Created on 15 April 2017, 21:36
 */

#ifndef DETIPIC32_H
#define	DETIPIC32_H

#ifdef	__cplusplus
extern "C" {
#endif

#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>


#define SYSCLK  80000000L   // System clock frequency, in Hz
#define PBUSCLK 40000000L   // Peripheral bus clock



#ifdef	__cplusplus
}
#endif

#endif	/* DETIPIC32_H */

