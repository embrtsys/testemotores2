/* 
 * File:   pfserio.h
 * Author: pf
 *
 * Created on 15 April 2017, 22:00
 */

#ifndef PFSERIO_H
#define	PFSERIO_H

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#ifdef	__cplusplus
extern "C" {
#endif

    
    
#define init_serial()      __XC_UART = 1   

int _mon_getc(int canblock);

#ifdef	__cplusplus
}
#endif

#endif	/* PFSERIO_H */

