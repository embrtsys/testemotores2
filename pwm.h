/**
 * \file pwm.h
 */


#ifndef PWM_H    /* Guard against multiple inclusion */
#define PWM_H

#include <xc.h>
#include <sys/attribs.h>
#include <stdint.h>
#include <stdbool.h>
#include "detipic32.h"


/* Define ports used for PWM */
#define PWM_EN      LATDbits.LATD4
#define PWM_IN1     LATDbits.LATD5
#define PWM_IN2     LATDbits.LATD6


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    /**
     * States of the PWM state machine
     */
    typedef enum {
        PWM_ON, /**< PWM is ON; outputs active */
        PWM_OFF /**< PWM is OFF; outputs disabled */
    } pwm_states_t;

    /**
     * Sets PWM frequency and dutycyle.
     * 
     * Sets PWM frequency and dutycycle. The module uses Timer2 and Output 
     * Compare 2 to generate the PWM signal. Two complementary signals are 
     * generated in PWM_IN1 and PWM_IN2 pins. 
     * 
     * @note The notation PWM_IN1 and PWM_IN2 refers to the pins in the motor control
     * circuit. These pins are outputs from the PIC microcontroller
     * 
     * @param pwm_frequency PWM frequency, in Hz
     * @param pwm_dutycycle PWM duty cycle as a percentage (0-99)
     * @return result code (currently always 0)
     */
    uint16_t setPWM(uint16_t pwm_frequency, uint16_t pwm_dutycycle);

    /**
     * Enables the PWM signals. Forces the PWM state machine to PWM_ON
     */
    void enable_pwm(void);

    /**
     * Disables the PWM signals. Forces the PWM state machine to PWM_OFF
     */
    void disable_pwm(void);

    /**
     * Gets the PWM state machine current state
     * @return Current state 
     */
    pwm_states_t get_pwm_state(void);



    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif